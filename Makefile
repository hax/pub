authorized_keys: sneak.keys jredbeard.keys goldibex.keys
	cat *.keys > authorized_keys

sneak.keys:
	curl https://sneak.cloud/authorized_keys > $@

goldibex.keys:
	curl https://github.com/goldibex.keys > $@

jredbeard.keys:
	curl https://github.com/jredbeard.keys > $@
